package com.fan.entity;

import lombok.Data;

import java.util.Date;

@Data
public class User {

    private Integer id;

    private  String name;

    private String sex;

    private int age;

    private Date createTime;

    public User(Integer id, String name, String sex, int age, Date createTime) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.createTime = createTime;
    }
}
