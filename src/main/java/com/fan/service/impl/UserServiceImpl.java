package com.fan.service.impl;

import com.fan.entity.User;
import com.fan.mapper.UserMapper;
import com.fan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public int batchInsertUser(List<User> users) {
        return userMapper.batchInsertUser(users);
    }

    @Override
    public User selectUserByCreateTime(Date createTime) {
        return userMapper.selectUserByCreateTime(createTime);
    }

    @Override
    public List<User> selecetByCreateTimeRange(Date startTime, Date endTime) {
        return userMapper.selecetByCreateTimeRange(startTime, endTime);
    }
}
