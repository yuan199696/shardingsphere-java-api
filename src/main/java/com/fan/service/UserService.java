package com.fan.service;

import com.fan.entity.User;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

public interface UserService {

    public int batchInsertUser(List<User> users);

    public User selectUserByCreateTime(Date createTime);

    public List<User> selecetByCreateTimeRange(Date startTime, Date endTime);
}
