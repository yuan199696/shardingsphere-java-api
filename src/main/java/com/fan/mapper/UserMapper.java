package com.fan.mapper;

import com.fan.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

@Mapper
public interface UserMapper {

    public int batchInsertUser(List<User> list);

    public User selectUserByCreateTime(Date createTime);

    public List<User> selecetByCreateTimeRange(Date startTime, Date endTime);
}
