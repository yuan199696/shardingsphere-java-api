package com.fan.controller;

import com.fan.entity.User;
import com.fan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class UserController {


    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    UserService userService;

    List<User> userList = new ArrayList<>();

    @PostConstruct
    public void initData() throws ParseException {
        userList.add(new User(1, "小明", "男", 20, simpleDateFormat.parse("2021-01-20 17:48:56")));
        userList.add(new User(2, "小花", "女", 21, simpleDateFormat.parse("2021-04-20 17:48:56")));
        userList.add(new User(3, "小张", "男", 22, simpleDateFormat.parse("2021-07-20 17:48:56")));
        userList.add(new User(4, "小李", "男", 23, simpleDateFormat.parse("2021-10-20 17:48:56")));
    }

    @GetMapping("/batchInsertUser")
    public int batchInsertUser() {
        return userService.batchInsertUser(userList);
    }

    @PostMapping("/selectUserByCreateTime")
    public User selectUserByCreateTime(String createTime) throws ParseException {
        Date date = simpleDateFormat.parse(createTime);
        return userService.selectUserByCreateTime(date);
    }

    @PostMapping("/selecetByCreateTimeRange")
    public List<User> selecetByCreateTimeRange(String startTime, String endTime) throws ParseException {
        return userService.selecetByCreateTimeRange(simpleDateFormat.parse(startTime),
                simpleDateFormat.parse(endTime));
    }
}
