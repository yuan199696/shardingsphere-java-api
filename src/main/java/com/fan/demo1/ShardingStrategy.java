package com.fan.demo1;

public enum ShardingStrategy {
	
	/**
	  * 标准分片策略
	 */
	STANDARD,
	
	/**
	  * 行表达式分片策略
	 */
	INLINE,
	
	/**
	  * 复合分片策略
	 */
	COMPLEX,
	
	/**
	 * Hint分片策略
	 */
	HINT;

}
