package com.fan.demo1;

import java.util.List;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "spring.shardingsphere")
@Data
public class ShardingSphereConfigProperties {

	/**
	 * 分片逻辑表的别名
	 */
	private List<String> dataSourceNames;

	/**
	  * 逻辑表
	 */
	private List<String> logicTable;
	
	/**
	  * 实际参与分片的数据表
	 */
	private List<String> actualDataNodes;
	
	/**
	  * 分片键
	 */
	private List<String> shardingColumns;
	
	/**
	  * 分片策略：不同分片策略需要自定义相应的分片算法
	 */
	private List<String> shardingStrategys;
	
	/**
	  * 分片算法：可以根据配置的包路径反射来加载对应的算法实现类
	 */
	private List<String> shardingAlgorithms;

	/**
	 * 关联表
	 */
	private List<String> bindingTableGroups;

	/**
	 * 是否显示sql
	 */
	private boolean sqlShow;

	/**
	 * 是否显示简单风格的sql
	 */
	private boolean sqlSimple;

	/**
	 * 数据库主机地址
	 */
	private String host;

	/**
	 * 数据库端口
	 */
	private String port;

	/**
	 * 数据库用户名
	 */
	private String username;

	/**
	 * 数据库密码
	 */
	private String password;

	/**
	 * 数据库名称
	 */
	private String dataSourceName;

}
