package com.fan.demo1;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;

import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Range;
/**
 * 
  * 标准分片策略：精准分片算法、范围分片算法的实现;
  * 此处对 user表 进行分片，分片键为 createTime;
  * 一年四张表按季度分表，如 202101、202102、202103、202104
 *
 */
public class PreciseAndRangShardingAlgorithm implements PreciseShardingAlgorithm<Date>, RangeShardingAlgorithm<Date> {

	private final Logger LOGGER = LoggerFactory.getLogger(PreciseAndRangShardingAlgorithm.class);

	private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
	
	/**
	  * 表的分隔符 
	 */
	private final String SPILT_SYMBOL = "_";
	

	@Override
	public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<Date> shardingValue) {
		Date createTime = shardingValue.getValue();
		if (null == createTime) {
			throw new RuntimeException("The shardingValue should not be null!");
		}
		if (availableTargetNames.size() <= 0) {
			throw new RuntimeException("There is no availableTargetTableNames!");
		}

		String formatDate = simpleDateFormat.format(createTime);
		String tableSuffix = formatDate.substring(0, 4) + "0" + getQuarter(createTime);
		
		for (String tableName : availableTargetNames) {
			if (tableName.endsWith(tableSuffix)) {
				return tableName;
			}
		}

		// 这里建议可以在数据库建一张逻辑表，如果数据无法匹配到具体的分表，那么就将数据插入到逻辑表中，避免数据的丢失
		// 也可以直接返回封装好的错误信息
		return shardingValue.getLogicTableName();
	}

	@Override
	public Collection<String> doSharding(Collection<String> availableTargetNames, RangeShardingValue<Date> shardingValue) {
		if (availableTargetNames.size() <= 0) {
			throw new RuntimeException("There is no availableTargetTableNames!");
		}
		
		Collection<String> resultTables = new LinkedHashSet<>(availableTargetNames.size());

		Range<Date> createTimeRange = shardingValue.getValueRange();
		if (!createTimeRange.hasLowerBound() && !createTimeRange.hasUpperBound()) {
			LOGGER.warn("The shardingValue need lower endpoint and upper endpoint!");
			return availableTargetNames;
		}

		Date lowerDate = createTimeRange.lowerEndpoint();
        Date upperDate = createTimeRange.upperEndpoint();
		String lowerDateStr = simpleDateFormat.format(lowerDate);
		String upperDateStr = simpleDateFormat.format(upperDate);

		if (lowerDateStr.compareTo(upperDateStr) > 0) {
			LOGGER.warn("The upper endpoint should be greater than lower endpoint");
			return availableTargetNames;
		}

		String suffixOfLower = lowerDateStr.substring(0, 4) + "0" + getQuarter(lowerDate);
		String suffixOfUpper = upperDateStr.substring(0, 4) + "0" + getQuarter(upperDate);

		for (String tableName : availableTargetNames) {
			String tableSuffix = tableName.substring(tableName.lastIndexOf(SPILT_SYMBOL) + 1);
			if (!tableName.equals(shardingValue.getLogicTableName())) {
				if (tableSuffix.compareTo(suffixOfLower) >= 0 && tableSuffix.compareTo(suffixOfUpper) <= 0) {
					resultTables.add(tableName);
				}
			}
		}
		
		return resultTables;
	}

	/**
	 * 获取月份对应的季度
	 *
	 * @param date
	 * @return
	 */
	public static int getQuarter(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return ((calendar.get(Calendar.MONTH) + 1) + 2) / 3;
	}
}
